use bevy::{prelude::*, sprite::Anchor};
use bevy_rapier2d::prelude::*;

#[allow(dead_code)]
#[derive(Resource)]
pub struct PlayerState {
    paused: bool,
    crouched: bool,
}

#[derive(Component)]
pub struct Player {
    speed: f32,
}

#[derive(Resource)]
pub struct PlayerControlOptions {
    left: KeyCode,
    right: KeyCode,
    up: KeyCode,
    down: KeyCode,
    crouch: KeyCode,
}

impl Default for PlayerControlOptions {
    fn default() -> Self {
        PlayerControlOptions {
            left: KeyCode::A,
            right: KeyCode::D,
            up: KeyCode::W,
            down: KeyCode::S,
            crouch: KeyCode::LControl,
        }
    }
}

pub struct PlayerPlugin {
    pub start_paused: bool,
}

fn player_keyboard_input(
    keyboard_input: Res<Input<KeyCode>>,
    controls: Res<PlayerControlOptions>,
    mut player_state: ResMut<PlayerState>,
    mut query: Query<(&mut Velocity, &mut Transform, &Player)>,
    mut window_query: Query<&Window>,
) {
    let (mut velocity, mut transform, player) = query.single_mut();

    let window = window_query.single_mut();

    if keyboard_input.pressed(controls.left) {
        velocity.linvel.x = -player.speed;
    } else if keyboard_input.pressed(controls.right) {
        velocity.linvel.x = player.speed;
    } else {
        velocity.linvel.x = 0.0;
    }

    if keyboard_input.pressed(controls.up) {
        velocity.linvel.y = player.speed;
    } else if keyboard_input.pressed(controls.down) {
        velocity.linvel.y = -player.speed;
    } else {
        velocity.linvel.y = 0.0;
    }

    if keyboard_input.just_pressed(controls.crouch) {
        player_state.crouched = true;
    } else if keyboard_input.just_released(controls.crouch) {
        player_state.crouched = false;
    }

    transform.translation.z = 2.0 - (1.0 + transform.translation.y / window.height());
}

fn player_startup(
    mut commands: Commands,
    mut _meshes: ResMut<Assets<Mesh>>,
    mut _materials: ResMut<Assets<ColorMaterial>>,
) {
    let x = 0.0;
    let y = 0.0;
    let width = 40.0;
    let height = 40.0;
    let collider_width = width / 2.0;
    let collider_height = height / 2.0 / 2.0;
    let collider = Collider::cuboid(collider_width, collider_height);
    commands.spawn((
        SpriteBundle {
            sprite: Sprite {
                color: Color::PINK,
                custom_size: Some(Vec2::new(width, height)),
                anchor: Anchor::Custom(Vec2::new(0.0, -0.25)),
                ..default()
            },
            transform: Transform::from_xyz(x, y, 2.0 - (1.0 + y / 600.0)),
            ..default()
        },
        RigidBody::Dynamic,
        collider,
        ActiveEvents::COLLISION_EVENTS,
        Velocity {
            linvel: Vec2::new(0.0, 0.0),
            angvel: 0.0,
        },
        ExternalImpulse {
            impulse: Vec2::new(0.0, 0.0),
            torque_impulse: 14.0,
        },
        LockedAxes::ROTATION_LOCKED,
        Friction {
            coefficient: 0.0,
            combine_rule: CoefficientCombineRule::Min,
        },
        GravityScale(0.0),
        Player { speed: 400.0 },
    ));

    let width_vert = 30.0;
    let height_vert = 200.0;
    let width_horz = 200.0;
    let height_horz = 30.0;
    let x = 0.0;
    let y = 0.0;
    let walls = vec![
        (
            x,
            y - height_horz / 2.0 - height_vert / 2.0,
            width_horz,
            height_horz,
        ),
        (
            x - width_horz / 2.0 + width_vert / 2.0,
            y,
            width_vert,
            height_vert,
        ),
        (
            x,
            y + height_vert / 2.0 + height_horz / 2.0,
            width_horz,
            height_horz,
        ),
        (
            x + width_horz / 2.0 - width_vert / 2.0,
            y,
            width_vert,
            height_vert,
        ),
    ];

    for (x, y, w, h) in walls.iter() {
        commands.spawn((
            SpriteBundle {
                sprite: Sprite {
                    color: Color::BLACK,
                    custom_size: Some(Vec2::new(*w, *h)),
                    ..default()
                },
                transform: Transform::from_xyz(*x, *y, 2.0 - (1.0 + *y / 600.0)),
                ..default()
            },
            RigidBody::Fixed,
            Collider::cuboid(w / 2.0, h / 2.0),
        ));
    }
}

fn player_crouch(player_state: Res<PlayerState>, mut query: Query<(&mut Transform, &Player)>) {
    for (mut transform, _) in query.iter_mut() {
        if player_state.crouched {
            transform.scale.x = 1.2;
            transform.scale.y = 0.5;
        } else {
            transform.scale.x = 1.0;
            transform.scale.y = 1.0;
        }
    }
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        let state = PlayerState {
            paused: self.start_paused,
            crouched: false,
        };
        let controls = PlayerControlOptions::default();
        app.insert_resource(state)
            .insert_resource(controls)
            .add_system(player_keyboard_input)
            .add_system(player_startup.on_startup())
            .add_system(player_crouch.run_if(resource_changed::<PlayerState>()));
    }
}
