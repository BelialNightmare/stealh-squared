use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_rapier2d::prelude::*;

#[allow(dead_code)]
#[derive(Component)]
pub struct EnemyState {
    paused: bool,
    awake: bool,
    speed: f32,
}

pub fn add_enemy(
    x: f32,
    y: f32,
    width: f32,
    height: f32,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes
                .add(shape::Quad::new(Vec2::new(width, height)).into())
                .into(),
            material: materials.add(ColorMaterial::from(Color::PURPLE)),
            transform: Transform::from_translation(Vec3::new(x, y, 0.0)),
            ..default()
        },
        RigidBody::Dynamic,
        Collider::cuboid(width / 2.0, height / 2.0),
        ActiveEvents::COLLISION_EVENTS,
        LockedAxes::ROTATION_LOCKED,
        Friction {
            coefficient: 0.0,
            combine_rule: CoefficientCombineRule::Min,
        },
        GravityScale(0.0),
        Player { speed: 400.0 },
    ));
}
